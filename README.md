![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

**Experimental Test-Bed for security testing of self-driving vehicles. **

The scope of this project is as follows:
- Build of MIT Duckietown for Self-Driving Vehicles
- Build of 3 x Autonomous Self-Driving small factor vehicles 
    -   1 x Duckiebot (Camera Sensor, using Duckie automation software)
    -   1 x DeepPi Car (Camera Sensor, using Tensor Flow for Deep Learning)
    -   1 x RobotCraft Bot 2017 (Infra Red and Sonar, uses Snap Programming)

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->



## Code



## Thesis 
(https://drive.google.com/file/d/10SvyxZMTYx4Cq9LlkOZJ4Mx0x4qA8wBk/view?usp=sharing)


